" Function to make the target for the recipe under the cursor
function! make#target#Make() abort

  " Declare list of targets to build
  let targets = []

  " Iterate back through the file starting at the current line looking for the
  " line with the target
  for li in reverse(range(1, line('.')))
    let line = getline(li)

    " If it matches the target format, we've found our line; split the targets
    " by space, and break
    let matchlist = matchlist(line, '^\([^:= \t][^:=]*\):')
    if len(matchlist)
      let targets = split(matchlist[1], '\s\+')
      break

    " If it wasn't the target line and doesn't have leading tabs, we're not in
    " a recipe block; break with an unset target
    elseif strpart(line, 0, 1) !=# "\t"
      break
    endif

  endfor

  " If we found targets, :make them; escape them if we can
  for target in targets
    if exists('*shellescape')
      let target = shellescape(target)
    endif
    execute 'make! -C %:p:h '.target
  endfor

endfunction
