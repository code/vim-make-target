make\_target.vim
================

This filetype plugin for Makefiles ("make" filetype) provides an autoload
function `make#target#Make()` and buffer-local mapping `<Plug>(MakeTarget)` to
`make!` the target for the recipe under the cursor, if it can be identified.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
